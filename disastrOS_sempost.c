#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include "disastrOS.h"
#include "disastrOS_syscalls.h"
#include "disastrOS_semaphore.h"
#include "disastrOS_semdescriptor.h"

void internal_semPost(){
	
	disastrOS_debug("\n---SEMPOST--- Entering:\n");
    int fd = running->syscall_args[0];
	
	//Prendo argomenti
    SemDescriptor* sem_des = (SemDescriptor*) SemDescriptorList_byFd(&running->sem_descriptors, fd);
    
    if (!sem_des){
		disastrOS_debug("\n---SEMPOST--- No such descriptor in the sem_descriptor list of the running process\n");
		running->syscall_retvalue = DSOS_ESEMPOST;
		return;
	}
	
    Semaphore* sem = (Semaphore*) sem_des->semaphore;
    
    if (!sem) {
		disastrOS_debug("\n---SEMPOST--- Error with the semaphore\n");
        running->syscall_retvalue = DSOS_ESEMPOST;
        return;
    }

    //Incremento contatore semaforo
    sem->count++;

    SemDescriptorPtr* desptr;
    
    //Se il contatore è maggiore di zero, allora si può risvegliare un processo
    
    if (sem->count > 0) {
		
        if (sem->waiting_descriptors.first) {
			disastrOS_debug("\n---SEMPOST--- Awakening a process\n");
			sem->count--;
            desptr = (SemDescriptorPtr*) List_detach(&sem->waiting_descriptors, (ListItem*) sem->waiting_descriptors.first);
            List_insert(&sem->descriptors, sem->descriptors.last, (ListItem*) desptr);
            
            sem_des = desptr->descriptor;
            
            
			PCB* next = sem_des->pcb; //metto in esecuzione il processo rimosso dalla waiting_list
			if (!next){
				disastrOS_debug("\n---SEMPOST--- Error! No process available\n");
				running->syscall_retvalue = DSOS_ESEMPOST;
				return;
			}
			next->status = Ready;
            List_detach(&waiting_list, (ListItem*) sem_des->pcb);
            List_insert(&ready_list, ready_list.last, (ListItem*) next);
            
            
        }
    }
    
    running->syscall_retvalue=0;
    disastrOS_debug("\n---SEMPOST--- Incredible success:\n");
    
    return;
    
}
