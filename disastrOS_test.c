#include <stdio.h>
#include <unistd.h>
#include <poll.h>
#include "disastrOS_constants.h"
#include "disastrOS.h"

#define BUFFER_LENGTH 5

#define ITERATION_COUNT 5
#define PROD_ROLE 0
#define CONS_ROLE 1

int filled_sem, empty_sem, read_sem, write_sem;

int buffer[BUFFER_LENGTH];
int write_index, read_index;

int funct = 0;

void enqueue(int x) {
	//disastrOS_printStatus();
	disastrOS_semWait(empty_sem);
	disastrOS_semWait(write_sem);    
	//disastrOS_printStatus();
    printf("[Child #%d] Writing value %d to position %d\n",disastrOS_getpid(),x,write_index);
    buffer[write_index] = x;
    printf("---Buffer[%d] value: %d\n\n",write_index,buffer[write_index]);
    write_index = (write_index + 1) % BUFFER_LENGTH;
    //disastrOS_printStatus();
    disastrOS_semPost(write_sem);
    disastrOS_semPost(filled_sem);
    //disastrOS_printStatus();
}

int dequeue() {
	int x;
	//disastrOS_printStatus();
	disastrOS_semWait(filled_sem);
    disastrOS_semWait(read_sem); 
    //disastrOS_printStatus();
    x = buffer[read_index];
    printf("[Child #%d] Reading value %d from position %d\n",disastrOS_getpid(),x, read_index);
    printf("---Buffer[%d] value: %d\n\n",read_index,buffer[read_index]);
    read_index = (read_index + 1) % BUFFER_LENGTH;	
    //disastrOS_printStatus();
    disastrOS_semPost(read_sem);
    disastrOS_semPost(empty_sem);
    //disastrOS_printStatus();
    return x;
}

// we need this to handle the sleep state
void sleeperFunction(void* args){
  printf("Hello, I am the sleeper, and I sleep %d\n",disastrOS_getpid());
  while(1) {
    getc(stdin);
    disastrOS_printStatus();
  }
}

void childFunction(void* args){
    printf("Hello, I am the child function %d\n",disastrOS_getpid());
  //OP SEMAPH
    empty_sem = disastrOS_semOpen(1, DSOS_CREATE, 0);
    filled_sem = disastrOS_semOpen(2, DSOS_CREATE, BUFFER_LENGTH);
    read_sem = disastrOS_semOpen(3, DSOS_CREATE, 1);
    write_sem = disastrOS_semOpen(4, DSOS_CREATE, 1);  
    int pid = disastrOS_getpid();
    int role = (disastrOS_getpid()%2)?PROD_ROLE:CONS_ROLE;
    int i;
  
    for (i=0; i < ITERATION_COUNT; i++) {
		if (role == PROD_ROLE) {
            int value = pid * i;
            enqueue(value);          
        } 
        else if (role == CONS_ROLE) {
            dequeue();
        } 
        else {
            printf("\n[Child #%d] Ruolo sconosciuto: %d\n", pid, role);
        }
    }

	printf("Child %d terminating\n", pid);
	disastrOS_semClose(filled_sem);
    disastrOS_semClose(empty_sem);
    disastrOS_semClose(read_sem);
    disastrOS_semClose(write_sem);
	disastrOS_exit(disastrOS_getpid());
}

void childFunctionProve(void* args){
  printf("Hello, I am the child function %d\n",disastrOS_getpid());
  printf("Opening semaphores\n");
  int sem1 = disastrOS_semOpen(1,DSOS_CREATE, 1); 
  printf("---SEM1 open--- returns: %d\n", sem1);
  
  int sem2 = disastrOS_semOpen(1,DSOS_CREATE|DSOS_EXCL,10); 
  printf("---SEM1 open (semaphore already existing and CREATE, EXCL specified)--- returns: %d\n", sem2);
  
  int sem3 = disastrOS_semOpen(2, DSOS_CREATE, 4);
  printf("---SEM3 open--- returns: %d\n", sem3);
  
  int sem4 = disastrOS_semOpen(3, DSOS_CREATE, -10);
  printf("---SEM4 open (value < 0)--- returns: %d\n", sem4);
  
  int p1 = disastrOS_semPost(sem2);
  printf("---SEM2 post (who is sem2?)--- returns: %d\n", p1);
  
  int c1 = disastrOS_semClose(sem1);
  printf("---SEM1 close--- returns: %d\n", c1);
  
  int c2 = disastrOS_semClose(sem2);
  printf("---SEM2 close(who is sem2?)--- returns: %d\n", c2);
  
  int c3 = disastrOS_semClose(sem3);
  printf("---SEM3 close--- returns: %d\n", c3);
  
  int c4 = disastrOS_semClose(sem3);
  printf("---SEM3 close(again)--- returns: %d\n", c4);
  
  disastrOS_exit(disastrOS_getpid());
}

void initFunctionProve(void* args){
    disastrOS_printStatus();
    printf("Hello, I am init and I just started\n");
    //disastrOS_spawn(sleeperFunction, 0);

/* NE FACCIO UNO SOLO
  printf("I feel like to spawn 10 nice threads\n");
  int alive_children=0;
  int i;
  for (i=0; i<10; ++i) {
    int type=0;
    int mode=DSOS_CREATE;
    printf("mode: %d\n", mode);
    printf("opening resource (and creating if necessary)\n");
    int fd=disastrOS_openResource(i,type,mode);
    printf("fd=%d\n", fd);
    disastrOS_spawn(childFunctionProve, 0);
    alive_children++;
  }
*/
    int alive_children=0;
    disastrOS_spawn(childFunctionProve, 0);
    alive_children++;
  
    disastrOS_printStatus();
    int retval;
    int pid;
    while(alive_children>0 && (pid=disastrOS_wait(0, &retval))>=0){
      //disastrOS_printStatus();
      printf("initFunction, child: %d terminated, retval:%d, alive: %d \n",
	     pid, retval, alive_children);
      --alive_children;
    }
    disastrOS_printStatus();
    printf("shutdown!\n");
    disastrOS_shutdown();
}

void initFunction(void* args) {
    disastrOS_printStatus();
    printf("Hello, I am init and I just started\n");
  //disastrOS_spawn(sleeperFunction, 0);

    printf("Buffer's initial state: [ ");
    for (int i=0; i<BUFFER_LENGTH; i++) printf("%d ",buffer[i]);
    
    printf("]\n");
    printf("I feel like to spawn 10 nice threads\n");
    int alive_children=0;
    
    for (int i=0; i<10; ++i) {
	printf(".");
	disastrOS_spawn(childFunction, 0);
    alive_children++;
    }
    
    printf("\nDone!\n");
    printf("My children will now read from or write to Buffy\n");
    disastrOS_printStatus();
    int retval;
    int pid;
    while(alive_children>0 && (pid=disastrOS_wait(0, &retval))>=0){
    //disastrOS_printStatus();
      printf("initFunction, child: %d terminated, retval:%d, alive: %d \n\n",
	     pid, retval, alive_children);
      --alive_children;
    }
 
    disastrOS_printStatus();
    printf("Buffer's final state:[ ");
    for (int i=0; i<BUFFER_LENGTH; i++) printf("%d ",buffer[i]);
    printf("]\n");
    printf("shutdown!\n");
    disastrOS_shutdown();
}

int main(int argc, char** argv){
    char* logfilename=0;
    if (argc>1) {
      logfilename=argv[1];
      funct = 1;
    } 
  // we create the init process processes
  // the first is in the running variable
  // the others are in the ready queue
  //printf("the function pointer is: %p", childFunction);
  // spawn an init process
  //printf("start\n");
    if (funct == 0)
	  disastrOS_start(initFunction, 0, logfilename);
    else
	  disastrOS_start(initFunctionProve, 0, logfilename);
    return 0;
}




