#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include "disastrOS.h"
#include "disastrOS_syscalls.h"
#include "disastrOS_semaphore.h"
#include "disastrOS_semdescriptor.h"

void internal_semClose(){

    disastrOS_debug("\n---SEMCLOSE--- Entering:\n");

    //Prendo argomenti dal pcb
    int fd = running->syscall_args[0];

	//Controllo se nella list dei descrittori dei semafori del processo ce n'è uno con quel fd. (DEVE ESSERCI ALTRIMENTI ERRORE)
    SemDescriptor* sem_des = SemDescriptorList_byFd(&running->sem_descriptors, fd);
    if (!sem_des){
	    disastrOS_debug("---SEMCLOSE--- The process has not the descriptor of the semaphore :%d\n",fd);
	    running->syscall_retvalue = DSOS_ESEMCLOSE;
	    return;
    }

	//Allora lo prendo e lo rimuovo dalla lista dei descrittori dei semafori del running
    sem_des = (SemDescriptor*) List_detach(&running->sem_descriptors, (ListItem*) sem_des);
    if (!sem_des) {
		disastrOS_debug("---SEMCLOSE--- Sem_des null\n");
		running->syscall_retvalue=DSOS_ESEMCLOSE;
		return;
	}

	//Prendo così il semaforo
    Semaphore* sem = sem_des->semaphore;

	//Rimuovo il puntatore al descrittore dalla lista dei semafori
    SemDescriptorPtr* desptr=(SemDescriptorPtr*) List_detach(&sem->descriptors, (ListItem*)(sem_des->ptr));
    if (!desptr) {
		disastrOS_debug("---SEMCLOSE--- Desptr null\n");
		running->syscall_retvalue=DSOS_ESEMCLOSE;
		return;
	}

    
    disastrOS_debug("---SEMCLOSE--- Closing the semaphore: Incredible success.\n");
    //Free di tutto
    SemDescriptor_free(sem_des);
    SemDescriptorPtr_free(desptr);
    running->last_sem_fd--;
    
    //Se tutti i processi hanno chiuso il semaforo lo elimino
    if (sem->descriptors.size == 0 && sem->waiting_descriptors.size==0){
	    disastrOS_debug("---SEMCLOSE--- Destroying the semaphore.\n");
	    sem = (Semaphore*) List_detach(&semaphores_list, (ListItem*)sem);
	    if (!sem) {
			disastrOS_debug("---SEMCLOSE--- Semaphore not found\n");
			running->syscall_retvalue=DSOS_ESEMCLOSE;
			return;
		}
	    
	    Semaphore_free(sem);
    }

    
    
    
    running->syscall_retvalue=0;
    
    
    
}
