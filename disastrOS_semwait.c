#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include "disastrOS.h"
#include "disastrOS_syscalls.h"
#include "disastrOS_semaphore.h"
#include "disastrOS_semdescriptor.h"

void internal_semWait(){

    disastrOS_debug("\n---SEMWAIT--- Entering:\n");
    int fd = running->syscall_args[0];

	//Deve andare a buon fine
    SemDescriptor* sem_des = (SemDescriptor*) SemDescriptorList_byFd(&running->sem_descriptors, fd);
    if (!sem_des){
        disastrOS_debug("\n---SEMWAIT--- No such descriptor in the sem_descriptor list of the running process\n");
        running->syscall_retvalue = DSOS_ESEMWAIT;
        return;
    }

    Semaphore* sem = sem_des->semaphore;
    if (!sem){
        disastrOS_debug("\n---SEMWAIT--- Error with the semaphore\n");
        running->syscall_retvalue = DSOS_ESEMWAIT;
        return;
    }
    /*
    Fonte: Wikipedia
    s--; //variabile del semaforo
    */
    SemDescriptorPtr* desptr = sem_des->ptr;
    

    /*if(s < 0 ){
        mette il Thread nella coda del Semaforo
        s++;
    */
    if (sem->count <= 0){
        disastrOS_debug("\n---SEMWAIT--- Not enough resources! The process will have to wait\n");
		
		//Rimuovo il descrittore del processo dalla lista dei descrittori
		List_detach(&sem->descriptors, (ListItem*) desptr);
     
        //Aggiungo il semaforo a quelli associati a processi in waiting tramite il puntatore al descrittore
        List_insert(&sem->waiting_descriptors, sem->waiting_descriptors.last, (ListItem*)desptr);
		
		
        
        PCB* run_next = (PCB*) List_detach(&ready_list, ready_list.first);
        if (!run_next){
            disastrOS_debug("\n---SEMWAIT--- No process is ready\n");
            running->syscall_retvalue = DSOS_ESEMWAIT;
            return;
        }
        //Metto in running il primo processo in ready e metto questo in waiting 
		List_insert(&waiting_list, waiting_list.last, (ListItem*)running);
		
        running->status = Waiting;
        
        running = run_next;
    }
    else{
		disastrOS_debug("\n---SEMWAIT--- Enough resources!\n");
		sem->count--;
	}
    disastrOS_debug("\n---SEMWAIT--- Incredible success:\n");
    running->syscall_retvalue = 0;
    return;


}
