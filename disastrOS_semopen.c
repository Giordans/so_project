#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include "disastrOS.h"
#include "disastrOS_syscalls.h"
#include "disastrOS_semaphore.h"
#include "disastrOS_semdescriptor.h"

void internal_semOpen(){

    disastrOS_debug("\n---SEMOPEN--- Entering\n");

    //Prendo gli argomenti dal running PCB
    int id = running->syscall_args[0];
    int mode = running->syscall_args[1];
    int value = running->syscall_args[2];

    if (value < 0){
		disastrOS_debug("---SEMOPEN--- ERROR! Semaphore value not valid!\n");
		running->syscall_retvalue = DSOS_ESEMOPEN;
		   return;
	}
	
   //Cerco nella lista dei semafori se è già presente
    Semaphore* sem = SemaphoreList_byId(&semaphores_list, id);
    SemDescriptor* sem_des;

     /*If O_CREAT is specified in oflag, then the semaphore is
       created if it does not already exist.
       If both O_CREAT and O_EXCL are specified in
       oflag, then an error is returned if a semaphore with the given name
       already exists.
     Fonte http://man7.org/linux/man-pages/man3/sem_open.3.html*/
	
	if ( (mode&DSOS_CREATE) == DSOS_CREATE){
		//C'è excl e il semaforo esiste?
		if ( (mode&DSOS_EXCL) == DSOS_EXCL && sem) {
			disastrOS_debug("---SEMOPEN--- ERROR! Semaphore already existing and DSOS_CREATE, DSOS_EXCL specified\n");
			running->syscall_retvalue = DSOS_ESEMOPEN;
		    return;
		}
		//Non esiste il semaforo?
		if(!sem){
		    sem = Semaphore_alloc(id, value);
			List_insert(&semaphores_list, semaphores_list.last, (ListItem*) sem);
		}
	}
	else {
		disastrOS_debug("---SEMOPEN--- Not valid argument!\n");
		running->syscall_retvalue = DSOS_ESEMOPEN;
	    return;
	
	}
	//Check su inconsistenza operazione
	if (!sem){
		disastrOS_debug("---SEMOPEN--- ERROR! Not able to alloc the semaphore\n");
		running->syscall_retvalue = DSOS_ESEMOPEN;
	    return;
	}

	//Creo semdescriptor per running process
	sem_des = SemDescriptor_alloc(running->last_sem_fd, sem, running);

	//Check su inconsistenza operazione
	if (!sem_des){
		disastrOS_debug("---SEMOPEN--- ERROR! Not able to alloc the descriptor for the running process\n");
		running->syscall_retvalue = DSOS_ESEMOPEN;
	    return;
	}

	//Aggiorno il numero di semafori aperti 
	running->last_sem_fd++;

	// Devo mettere il descrittore nella lista dei descrittori del processo.
	SemDescriptorPtr* des_ptr = SemDescriptorPtr_alloc(sem_des);
	sem_des->ptr=des_ptr;
    List_insert(&running->sem_descriptors, running->sem_descriptors.last, (ListItem*)sem_des);
	List_insert(&sem->descriptors, sem->descriptors.last, (ListItem*)des_ptr);
	running->syscall_retvalue = sem_des->fd;

	disastrOS_debug("---SEMOPEN--- Incredible success!\n");
    return;

}
